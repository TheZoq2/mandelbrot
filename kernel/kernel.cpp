constexpr int num_iterations = 23;

constexpr double WIDTH = 640;
constexpr double HEIGHT = 480;

#define MULTI_MODE

// #if 1
// #include <cmath>
// 
// #define limestone_min fmin
// #define limestone_max fmax
// #endif

struct Complex {
    double i;
    double r;
};

constexpr __attribute__((always_inline)) inline Complex cmult(Complex x, Complex y) {
    return Complex{
        x.r*y.r - x.i*y.i,
        x.r*y.i + y.r*x.i
    };
}

constexpr __attribute__((always_inline)) inline Complex cadd(Complex x, Complex y) {
    return Complex{ x.r + y.r, x.i + y.i };
}

double mandelbrot_kernel(
    double x,
    double y,
    double center_x,
    double center_y,
    double zoom_amount
#ifdef MULTI_MODE
    , double mode
#endif
) {
    declare_bounds(x, 0, WIDTH);
    declare_bounds(y, 0, HEIGHT);
    declare_bounds(center_x, -2, 2);
    declare_bounds(center_y, -2, 2);
    declare_bounds(zoom_amount, 0, 0.1);
#ifdef MULTI_MODE
    declare_bounds(mode, 0, 10);
#endif

    double zoom_factor = 1 - zoom_amount;

    double r_ = (x / WIDTH * 4 - 2) * zoom_factor + center_x;
    double i_ = (y / HEIGHT * 4 - 2) * zoom_factor + center_y;

    double r = limestone_min(2, limestone_max(-2, r_));
    double i = limestone_min(2, limestone_max(-2, i_));

    Complex c{r, i};
    Complex z{0, 0};

    double result = 1;

    #pragma unroll
    for(int i = 0; i < num_iterations; i++) {
        z = cadd(cmult(z, z), c);

        if (z.r > 2 || z.i > 2 || z.r < -2 || z.i < -2) {
            result = 0;
            return result;
            // break;
        }

        double nzr = limestone_min(2, limestone_max(-2, z.r));
        double nzi = limestone_min(2, limestone_max(-2, z.i));
#ifndef MULTI_MODE
        z.r = nzr;
        z.i = nzi;
#else
        // z = Complex{z.r, z.i};
        if(mode == 0) {
            z.r = nzr;
            z.i = nzi;
        }
        else if (mode == 1) {
            z.r = nzi;
            z.i = nzr;
        }
        else if (mode == 2) {
            if(i % 3 == 0) {
                z.r = nzr;
                z.i = nzi;
            }
            else {
                z.r = nzi;
                z.i = nzr;
            }
        }
        // else if (mode == 3) {
        //     if (i < 10) {
        //         z.r = nzr;
        //         z.i = nzi;
        //     }
        //     else {
        //         z.i = nzr;
        //         z.r = nzi;
        //     }
        // }
        else {
            z.r = nzi;
            z.i = nzr;
        }
#endif
    }
    return result;
}
