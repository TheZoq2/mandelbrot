#include "build/_module_/_module_.hpp"
// #include "kernel.cpp"
#include <iostream>


int main() {
    for(int x = 0; x < 640; x += 5) {
        for(int y = 0; y < 480; y += 5) {
            auto c = _module__input{x, y};

            auto result = _module_(c);

            // auto result = mandelbrot_kernel(x, y);

            std::cout << (result.__0 == 0 ? "#" : " ");
        }
        std::cout << "" << std::endl;
    }
}
