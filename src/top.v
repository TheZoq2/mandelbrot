// look in pins.pcf for all the pin names on the TinyFPGA BX board
module top (
    input clk,

    input[7:0] pmod4,
    output[7:0] pmod7
);
    // assign rst = pmod0[2];

    reg rst = 1;
    always @(posedge clk) begin
        rst <= 0;
    end

    wire vsync, hsync, r, g, b;

    assign pmod7[0] = vsync;
    assign pmod7[1] = hsync;

    assign pmod7[4] = r;
    assign pmod7[5] = g;
    assign pmod7[6] = b;

    wire up, down, left, right, depress, zin, zout, b3;

    assign {depress, down, left, up, right, zout, b3, zin} = pmod4;

    main main
        ( ._i_clk(clk)
        , ._i_rst(rst)
        , ._i_down_unsync(down)
        , ._i_left_unsync(left)
        , ._i_up_unsync(up)
        , ._i_right_unsync(right)
        , ._i_zoom_out_unsync(zout)
        , ._i_zoom_in_unsync(zin)
        , ._i_extra_button_unsync(b3)
        , .__output({hsync, vsync, r, g, b})
        );
endmodule

